# GNOME.org evaluation

Content evaluation of the gnome.org current state, suggestions of what needs to be changed and initial goals for a website update.

I used a lot the [GTK website](https://www.gtk.org/) as inspiration to evaluate the content and to make suggestions in order to keep the consistency of the websites in the future update.

This evaluation covers each page separately to keep information organized and clear and to facilitate tracking later discussions. For each page, there is an abstract about in what the page consists now, how it functions, how the content is placed and what could be improved.

* [Main](pages/main-page.md)
* GNOME 3
* Get GNOME
* Technologies
* Get Involved
* Foundation
    * The GNOME Foundation
    * Staff
    * Governance
    * Legal and Trademarks
    * Reports
    * Membership
    * Contact Us
* About Us
* Donate
* Contact (different from the Foundation contact)
* Merchandise
* Press Releases

Apart from each page, there is an evaluation of the header and the footer following the same thoughts as the pages evaluations.

## General considerations
## Suggestions