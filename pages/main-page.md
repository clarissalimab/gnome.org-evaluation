[Page link](https://www.gnome.org/)

[Page screenshot](../images/main-page.png)

# Main
Presents an image of a computer with an outdated GNOME showing some applications and gives a brief description about GNOME. There is a button [**Learn More**](gnome-3.md) about GNOME and a button to [**Get GNOME**](getting-gnome.md).

Below, there is a section with two links for people interested in helping GNOME with a donation ([**Make a donation and become a Friend of GNOME!**](support-gnome.md)) or getting involved ([**Get involved!**](get-involved.md)), and a short explanation about both of the options.

Last, the page shows the three latest articles with the date, the title and the beginning of the text of each one of the presented articles. After the articles, there is a button [**News Archives**](news.md) for the user to see more news.

## Evaluation
GNOME's presentation is too superficial and doesn't say anything about what it really is. I agree that simple is better, but it would also be simpler for a new user if they didn't have to click on "Learn more" to understand what GNOME is.

The page seems a little timeless and doesn't value the latest GNOME release (e.g. the screenshot isn't from the latest GNOME version). It does not have images to create a friendly and illustrative idea.

## Suggestions
* GNOME Presentation
    * Emphasize the latest GNOME version (the name, number)
    * Show more screenshots of GNOME
    * Describe more GNOME's advantages
    * Remove the "Learn more" button and create a "Features" link in the navbar
    * Create a section with the GNOME programs, displaying their names and icons and linking them to their respective wiki
* Get involved
    * Create illustrations about donating and contributing to GNOME
* News
    * Put images on the articles (if the article doesn't have any image, there could be some generic images to use just to call more the attention)
    * The button **News Archives** could be a link labeled "**Show more news »**" instead

